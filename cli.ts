#!/usr/bin/env node

const path = require('path');
const fs = require('fs');

const commander = require('commander');
const checker = require('istanbul-threshold-checker');

import { TypeResult } from './types';

commander
    .arguments('<coverageJson>')
    .option('--global <n>', 'Set global thresholds', parseThresholdArg)
    .option('--each <n>', 'Set thresholds every file should meet', parseThresholdArg)
    .action((coverageJson: any, options: any) => {
        const coverageJsonPath = path.resolve(coverageJson);
        if(!fs.existsSync(coverageJsonPath)) {
            console.warn('[istanbul-threshold-exit-code] Error: coverage JSON file not found at', coverageJsonPath);
            process.exit(1);
        }
        if(
            typeof options.global === 'undefined' &&
            typeof options.each === 'undefined'
        ) {
            console.warn('[istanbul-threshold-exit-code] Error: No thresholds have been defined - unable to successfully check whether they are met');
            process.exit(1);
        }
        const coverageObj = require(coverageJsonPath);

        const thresholds = {
            global: options.global,
            each: options.each
        };

        const results: TypeResult[] = checker.checkFailures(thresholds, coverageObj);

        const success = results.filter(typeResult => !isSuccess(typeResult)).length === 0;

        if(success) {
            process.exit(0);
        }
        process.exit(1);
    })
    .parse(process.argv);

function isSuccess(typeResult: TypeResult) {
    return (typeof typeResult.global === 'undefined' || typeResult.global.failed === false)
        && (typeof typeResult.each === 'undefined' || typeResult.each.failed === false);
}
function parseThresholdArg(arg: string) {
    if(arg.indexOf(',') === -1) {
        return Number.parseInt(arg, 10);
    }
    const thresholds = arg.split(',').map(threshold => Number.parseInt(threshold, 10));
    return {
        statements: thresholds[0],
        branches: thresholds[1],
        lines: thresholds[2],
        functions: thresholds[3]
    };
}
