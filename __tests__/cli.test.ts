const child_process = require('child_process');

/* Note: this coverage JSON includes global coverage of:
 * - Statements: 95.32
 * - Branches: 77.36
 * - Lines: 97.28
 * - Functions: 90.7
 */
const fixture = './__tests__/__fixtures__/coverage-95,77,97,90.json';

test('Error when pointing to a non-existent coverage JSON', () => {
    expect(() => child_process.execSync('node ./dist/cli.js nonexistent.json')).toThrow(/coverage JSON file not found/);
});

test('Error when providing neither `global` nor `each` thresholds', () => {
    expect(() => child_process.execSync(`node ./dist/cli.js ${fixture}`)).toThrow(/No thresholds have been defined/);
});

test('No error when providing just `global` thresholds', () => {
    expect(() => child_process.execSync(`node ./dist/cli.js ${fixture} --global 10`)).not.toThrow();
});

test('No error when providing just `each` thresholds', () => {
    expect(() => child_process.execSync(`node ./dist/cli.js ${fixture} --each 10`)).not.toThrow();
});

test('Meeting thresholds should not result in an error exit code', () => {
    expect(() => child_process.execSync(`node ./dist/cli.js ${fixture} --global 10`)).not.toThrow();
});


test('Not meeting thresholds should result in an error exit code', () => {
    expect(() => child_process.execSync(`node ./dist/cli.js ${fixture} --global 100`)).toThrow();
});

test('Thresholds should be able to be specified for statements, branches, lines and functions', () => {
    expect(() => child_process.execSync(`node ./dist/cli.js ${fixture} --global 95,77,97,90`)).not.toThrow();
});

test('Excessive thresholds should be able to be specified for statements', () => {
    expect(() => child_process.execSync(`node ./dist/cli.js ${fixture} --global 96,77,97,90`)).toThrow();
});

test('Excessive thresholds should be able to be specified for branches', () => {
    expect(() => child_process.execSync(`node ./dist/cli.js ${fixture} --global 95,78,97,90`)).toThrow();
});

test('Excessive thresholds should be able to be specified for lines', () => {
    expect(() => child_process.execSync(`node ./dist/cli.js ${fixture} --global 95,77,98,90`)).toThrow();
});

test('Excessive thresholds should be able to be specified for functions', () => {
    expect(() => child_process.execSync(`node ./dist/cli.js ${fixture} --global 95,77,97,91`)).toThrow();
});
