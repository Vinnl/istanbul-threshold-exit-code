CLI tool that can be passed the path to the Istanbul JSON reporter's output, and will exit with a status code of 0 or 1, respectively, based on whether the reporter's output does or does not meet given thresholds.

The primary use case is to be run in CI. The main trigger for writing this was [Jest](https://facebook.github.io/jest/)'s built-in coverage threshold checker [not recognising remapped (e.g. to account for TypeScript) coverage](https://github.com/kulshekhar/ts-jest/issues/42#issuecomment-256792842). That issue [has been solved with the release of Jest 20](https://github.com/kulshekhar/ts-jest/pull/200), though.

# Usage

The recommended way to use this is to install it locally in your project, then to add the following to one of the `scripts` in your `package.json`:

```bash
istanbul-threshold-exit-code path/to/coverage.json [--global <global thresholds>] [--each <per-file thresholds>]
```

# Arguments

## `--global`

Thresholds that all files taken together should meet. Either a comma-separated list of (in that order) statement, branch, line and function coverage thresholds, or a single number for all of them. Threshold values can be [any value supported by istanbul-threshold-checker](https://www.npmjs.com/package/istanbul-threshold-checker#thresholds).  

Examples:

```bash
# require 100% statement and line coverage, but just 90% for branches and functions
istanbul-threshold-exit-code path/to/coverage.json --global 100,90,100,90

# require 80% coverage for everything
istanbul-threshold-exit-code path/to/coverage.json --global 100
```

## `--each`

Thresholds that each individual file should meet. Either a comma-separated list of (in that order) statement, branch, line and function coverage thresholds, or a single number for all of them. Threshold values can be [any value supported by istanbul-threshold-checker](https://www.npmjs.com/package/istanbul-threshold-checker#thresholds).  

Examples:

```bash
# require 100% statement and line coverage, but just 90% for branches and functions
istanbul-threshold-exit-code path/to/coverage.json --each 100,90,100,90

# require 80% coverage for everything
istanbul-threshold-exit-code path/to/coverage.json --each 100
```
