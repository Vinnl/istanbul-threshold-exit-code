export interface TypeResult {
    type: string;
    global?: {
        failed: boolean;
        value: number;
    },
    each?: {
        failed: boolean;
        failures: string[];
    }
}